package system;

import view.MenuFrame;

public class MahjongSystem {

	/**
	 * Méthode de lancement.
	 * 
	 * @param args default main argument
	 */
	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {
				new MenuFrame();
			}
		});
	}
}
