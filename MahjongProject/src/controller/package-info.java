/**
 * Ce package contient toutes le classe du controller utilisé :
 * <ul>
 * <li>La classe <code>MahjongController.java</code></li>
 * </ul>
 * 
 * @author Dylan Brossel
 */
package controller;