/**
 * Ce package contient toutes les classes réseau utilisées :
 * <ul>
 * <li>La classe <code>Client.java</code></li>
 * <li>La classe <code>Host.java</code></li>
 * <li>La classe <code>PlayerSocket.java</code></li>
 * </ul>
 * 
 * @author Dylan Brossel
 */
package network;