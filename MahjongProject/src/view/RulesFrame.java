package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 * Frame contenant les règles de bases.
 * 
 * @author Dylan
 *
 */
@SuppressWarnings("serial")
public class RulesFrame extends JFrame implements ActionListener{
	
	private JButton closeFrame = new JButton("Fermer");
	private JTextArea chat = new JTextArea("\n---------------------------------------- Règles des bases du Mahjong Japonais. ------------------------------------------\n");
	
	public RulesFrame() {
		super("Règles du jeu");
		
		this.getContentPane().setLayout(new BorderLayout());
		Box bv = Box.createVerticalBox();
		
		chat.append("\n - Pour avoir une main gagnante il faut 4 combinaisons de 3 tuiles soit des suites, avec les chiffres de\n"
				+ " la même « couleur », soit des brelans, trois tuiles identiques. Plus une paire identique. Ce qui fait 14\n"
				+ " tuiles.\n");
		chat.append("\n - Une main comporte 13 tuiles et se veut cachée de la vue des autres joueurs.\n");
		chat.append("\n - Les actions possibles sont :\n");
		chat.append("\n     - Déclaration de victoire par pioche, déclaré « tsumo », ou rejet, déclaré « ron », d'un autre joueur.\n");
		chat.append("\n     - La pioche, suivis d'un rejet si la main n'est pas gagnante.\n");
		chat.append("\n     - L'appropriation d'un rejet, et uniquement au moment de celui-ci, déclaré en « pon » si la tuiles sert\n"
				+ "       à la constitution d'un brelan complet. Déclaré en « chi » si la tuiles sert à la constitution d'une suite\n"
				+ "       complète, 3 tuiles, MAIS déclaration possible uniquement par le joueur qui suit dans le cas d'une\n"
				+ "       suite.\n");
		chat.append("\n - Priorité des appels, « ron » étant le plus prioritaire: « ron », « pon », « chi ».\n");
		chat.append("\n - tl;dr \"Pon\" : 3 tuiles identiques, \"Chi\" : 3 tuiles qui se suivent et \"Ron\" : main gagnante.");
		chat.setEditable(false);
		chat.setLineWrap(true);
		bv.add(chat);
		closeFrame.addActionListener(this);
		bv.add(closeFrame);
		
		this.add(bv);
		this.setSize(576, 480);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
	}

}
