package view;

import javax.swing.JPanel;

import controller.MahjongController;
import model.Player;

/**
 * Zone contenant les tuiles du joueur. On peut deplacer les tuiles via un drag and drop pour mieux les arranger. 
 * Il est également possible de faire glisser jusqu'au centre pour défausser une tuile (action possible uniquement au tour du joueur).
 * 
 * Tech : flowLayout entre les 3 panel : largeur differente mais hauteur identique.
 * 
 * @author Martin
 * 
 */
@SuppressWarnings("serial")
public class PlayerPanel extends JPanel{

	private Player player;
	
	private VisibleTilePanel vtp;
	private DragDropPanel ddp;
	
	public PlayerPanel(int width, int height, MahjongController mc) {
		super();
		
		this.setVtp(new VisibleTilePanel('S'));
		this.setDdp(new DragDropPanel(mc));
		this.setLayout(null);
		this.add(ddp);
		ddp.setBounds(0, 0, width-192, 256);
		ddp.setOpaque(false);
		this.add(vtp);
		vtp.setBounds(width-192, 0, 192, 256);
		vtp.setOpaque(false);
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public VisibleTilePanel getVtp() {
		return vtp;
	}

	public void setVtp(VisibleTilePanel vtp) {
		this.vtp = vtp;
	}

	public DragDropPanel getDdp() {
		return ddp;
	}

	public void setDdp(DragDropPanel ddp) {
		this.ddp = ddp;
	}

}
