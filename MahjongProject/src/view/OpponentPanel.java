package view;

import javax.swing.JPanel;

import Util.TileUtil;
import model.Tile;

/**
 * Panel commun à tous les adversaires mais s'affiche différemment s'il est à gauche, droite ou en face.
 * 
 * @author Martin
 * 
 */
@SuppressWarnings("serial")
public class OpponentPanel extends JPanel{
	private char direction;
	private VisibleTilePanel vtp;
	private JPanel discardPanel;
	
	private int nbTile = 0;
	
	public OpponentPanel(char direction, int width, int height) {
		super();
		this.setDirection(direction);
		
		this.setLayout(null);
		
		discardPanel = new JPanel();
		discardPanel.setLayout(null);
		discardPanel.setOpaque(false);
		
		vtp = new VisibleTilePanel(direction);
		vtp.setOpaque(false);
		
		if(direction == 'W'){
			discardPanel.setBounds(0, 0, 256, height-192-256-TileUtil.MARGIN);
			vtp.setBounds(0, height-192-256-TileUtil.MARGIN, 256, 192);
		}
		else if(direction == 'N'){
			discardPanel.setBounds(192, 0, width-256-192-256, 256);
			vtp.setBounds(0, 0, 192, 256);
		}
		else{
			discardPanel.setBounds(0, 192, 256, height-192-256);
			vtp.setBounds(0, 0, 256, 192);
		}	
		
		this.add(discardPanel);
		this.add(vtp);
		
	}
	public char getDirection() {
		return direction;
	}
	public void setDirection(char direction) {
		this.direction = direction;
	}
	public VisibleTilePanel getVtp() {
		return vtp;
	}
	public void setVtp(VisibleTilePanel vtp) {
		this.vtp = vtp;
	}
	public JPanel getDiscardPanel() {
		return discardPanel;
	}
	public void setDiscardPanel(JPanel discardPanel) {
		this.discardPanel = discardPanel;
	}
	
	/**
	 * Ajoute une tuile à la défausse du joueur.
	 * 
	 * @param t tuile à défausser.
	 */
	public void addDiscardedTile(Tile t){
		TileComponent tc = new TileComponent(t, direction, true);
		int tileSize = TileUtil.tileSizeDP;
		tc.setSize(tileSize, tileSize);
		tc.setDraggable(false);
		if(direction == 'N'){
			tc.setLocation((nbTile - ((nbTile/6)*6))*56 - tileSize/5, (nbTile/6)*tileSize);
		}
		else if(direction == 'E'){
			tc.setLocation((nbTile/9)*tileSize,(nbTile - ((nbTile/9)*9))*56 - tileSize/5);
		}
		else{
			tc.setLocation((nbTile/9)*tileSize,(nbTile - ((nbTile/9)*9))*56 - tileSize/5);
		}
		discardPanel.add(tc);
		this.repaint();
		
		nbTile++;
	}

}
