package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import model.Tile;

/**
 * Frame affichée en cas de victoire de l'un des joueurs. Fin de la partie avec affichage du joueur et de la main gagnante.
 * 
 * @author Dylan
 *
 */
@SuppressWarnings("serial")
public class VictoryFrame extends JFrame implements ActionListener{
	
	private JButton closeFrame = new JButton("Fermer");
	private JLabel[] tiles = new JLabel[14];
	private JLabel text = new JLabel("\n---------- Fin de partie ----------");
	
	public VictoryFrame(String pseudo, ArrayList<Tile> hand) {
		super("Fin de partie");
		
		this.getContentPane().setLayout(new BorderLayout());
		Box bv = Box.createVerticalBox();
		Box bh = Box.createHorizontalBox();
		Box bh2 = Box.createHorizontalBox();
		
		JLabel text2 = new JLabel("\n       Victoire de "+pseudo+" !");
		
		bv.add(text);
		bv.add(text2);
		for(int i = 0; i<14; i++){
			tiles[i] = new JLabel( new ImageIcon(hand.get(i).getImage()));
			if(i<7) bh.add(tiles[i]);
			if(i>=7) bh2.add(tiles[i]);
		}
		bv.add(bh);
		bv.add(bh2);
		closeFrame.addActionListener(this);
		bv.add(closeFrame);
		
		this.add(bv);
		this.setSize(960, 400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
	}

}
