package view;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import Util.TileUtil;
import model.Table;
import model.Tile;

/**
 * Panel contenant le mur et la dernière tuile défaussée.
 * 
 * @author Martin
 *
 */
@SuppressWarnings("serial")
public class WallPanel extends JPanel{
	private TileComponent lastTile;
	private ArrayList<Tile> wall = new ArrayList<Tile>();
	private ArrayList<TileComponent> tc = new ArrayList<TileComponent>();
	private JPanel lastTilePanel = new JPanel();
	private Table table;
	private int w;
	private int h;
	public WallPanel(Table table, int w, int h) {
		this.setLayout(null);
		this.setBounds(256, 256, w -256*2, h-256*2 - TileUtil.MARGIN);
		this.setOpaque(false);
		this.setTable(table);
		this.h = h;
		this.w = w;

		this.setBorder(BorderFactory.createLineBorder(Color.black));
		
		lastTilePanel.setLayout(null);
		lastTilePanel.setOpaque(false);
		lastTilePanel.setBounds((w-256-256-128)/2, (h-256-256-TileUtil.MARGIN-128)/2, 128, 128);
		lastTilePanel.setBorder(BorderFactory.createTitledBorder("LastTile : "));
		this.add(lastTilePanel);
	}
	public TileComponent getLastTile() {
		return lastTile;
	}
	public void setLastTile(TileComponent lastTile) {
		this.lastTile = lastTile;
	}
	public ArrayList<Tile> getWall() {
		return wall;
	}
	
	/**
	 *  Affichage du mur.
	 * 
	 * @param wall liste de tuile du mur.
	 */
	public void setWall(ArrayList<Tile> wall) {
		this.wall = new ArrayList<Tile>(wall);
		int tileSize = TileUtil.tileSizeWall;
		
		int i = 0;
		while(i < wall.size()){
			TileComponent t1 = null, t2 = null;
			switch(i/34){
				case 0:{
					t1 = new TileComponent(wall.get(i),'E',false);
					t1.setBounds(w-256 - 256 - 65, (i/2)*20 + 64, tileSize, tileSize);
					i++;
					t2 = new TileComponent(wall.get(i),'E',false);
					t2.setBounds(w-256 - 256 - 69, (i/2)*20 + 64, tileSize, tileSize);
					i++;
				}
				break;
				case 1:{
					t1 = new TileComponent(wall.get(i),'S',false);
					t1.setBounds(w - ((i-34)/2*20 + 80), h-256-256-TileUtil.MARGIN-64, tileSize, tileSize);
					i++;
					t2 = new TileComponent(wall.get(i),'S',false);
					t2.setBounds(w- ((i-34)/2*20 +256 +256 + 96 + 16), h-256-256-TileUtil.MARGIN-69, tileSize, tileSize);
					i++;
				}
				break;
				case 2:{
					t1 = new TileComponent(wall.get(i),'W',false);
					t1.setBounds(33,h - ((i-68)/2*20 + 90 +256 +256 + TileUtil.MARGIN), tileSize, tileSize); 
					i++;
					t2 = new TileComponent(wall.get(i),'W',false);
					t2.setBounds(37, h - ((i-68)/2*20 + 90 +256 +256 + TileUtil.MARGIN), tileSize, tileSize);
					i++;
				}
				break;
				case 3:{
					t1 = new TileComponent(wall.get(i),'N',false);
					t1.setBounds((i-102)/2*20 + 80, 33, tileSize, tileSize);
					i++;
					t2 = new TileComponent(wall.get(i),'N',false);
					t2.setBounds((i-102)/2*20 + 80, 37, tileSize, tileSize);
					i++;
				}
			}
			this.add(t1);
			this.add(t2);
			tc.add(t1);
			tc.add(t2);
			t1.setDraggable(false);
			t2.setDraggable(false);
		}

		this.repaint();
	}

	/**
	 * Supprime une tuile du mur.
	 * 
	 * @param t la tuile à supprimer.
	 */
	public void removeTile(Tile t){
		for(int i = 0; i < tc.size() ; i++){
			if(t.equals(tc.get(i).getTile())){
				this.remove(tc.get(i));
				wall.remove(t);
				this.repaint();
			}
		}
	}
	
	/**
	 * Met à jour la dernière tuile défaussée.
	 * 
	 * @param t dernière tuile défaussée.
	 */
	public void updateLastTile(Tile t){
		lastTilePanel.removeAll();
		if(t != null){
			this.setLastTile(new TileComponent(t,'S',true));
			lastTile.setBounds(16, 16, TileUtil.tileSizeHand, TileUtil.tileSizeHand);
			lastTile.setDraggable(false);
			lastTilePanel.add(lastTile);
		}

		lastTilePanel.repaint();
	}
	
	public Table getTable() {
		return table;
	}
	public void setTable(Table table) {
		this.table = table;
	}

}
