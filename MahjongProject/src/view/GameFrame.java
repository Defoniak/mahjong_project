package view;

import java.awt.Dimension;

import javax.swing.JFrame;

import controller.MahjongController;
import model.Player;

/**
 * C'est la fenetre de jeu. Il devra contenir tous les differents panels et en faire un tout.
 * Il chargera la table : son image, le mur, la defausse et la derniere tuile jouee.
 * Il chargera les joueurs : leurs mains.
 * Il chargera les tuiles : leurs images.
 * 
 * @author Martin
 * 
 */
@SuppressWarnings("serial")
public class GameFrame extends JFrame{
	
	private MahjongController mc;
	private TablePanel tp;
	private SidePanel sp;
	
	private Player player;

	public GameFrame(MahjongController mc) {
		super("MahjongProject");
		
		this.setMc(mc);
		
		Dimension tailleEcran = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		int hauteur = (int)tailleEcran.getHeight();
		@SuppressWarnings("unused")
		int largeur = (int)tailleEcran.getWidth();
		int h = hauteur - hauteur/20;
		int w = (int)((100*h)/75);
		
		setSize(w, h);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
	    tp = new TablePanel(mc, h, h);
	    sp = new SidePanel(w,h,mc);
	    this.setLayout(null);
	    this.add(tp);
	    this.add(sp);
		
		this.setVisible(true);
	}

	public TablePanel getTp() {
		return tp;
	}

	public void setTp(TablePanel tp) {
		this.tp = tp;
	}

	public SidePanel getSp() {
		return sp;
	}

	public void setSp(SidePanel sp) {
		this.sp = sp;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public MahjongController getMc() {
		return mc;
	}

	public void setMc(MahjongController mc) {
		this.mc = mc;
	}
}
