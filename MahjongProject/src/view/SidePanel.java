package view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import Util.TileUtil;
import controller.MahjongController;
import model.Tile;

/**
 * Panel de droite contenant le "chat", les boutons et la défausse.
 * 
 * @author Martin
 *
 */
@SuppressWarnings("serial")
public class SidePanel extends JPanel implements ActionListener{
	private JLabel pseudoLabel = new JLabel("Joueur actuel : ");
	private JTextArea chat = new JTextArea("Bienvenue dans Mahjong Project.");
	private JPanel discardPanel;
	
	private JButton pon = new JButton("Pon");
	private JButton ron = new JButton("Ron");
	private JButton chi = new JButton("Chi");
	private JButton help = new JButton("Aide");
	private JButton rules = new JButton("Règles");
	
	int nbTile = 0;
	
	private MahjongController mc;
	
	public SidePanel(int w, int h, MahjongController mc) {
		this.setBounds(h + 10, 10, w - h - 40, h);
		
		this.mc = mc;
		
		Box bv = Box.createVerticalBox();
		Component separation = Box.createRigidArea(new Dimension(0,20));
		Component separation2 = Box.createRigidArea(new Dimension(0,20));
		Component separation3 = Box.createRigidArea(new Dimension(0,20));
		Component separation4 = Box.createRigidArea(new Dimension(0,20));
		pseudoLabel.setText(pseudoLabel.getText());
		pseudoLabel.setAlignmentX(0.5f);
		chat.setEditable(false);
		chat.setLineWrap(true);
		JScrollPane sp = new JScrollPane(chat);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		sp.setBorder(BorderFactory.createTitledBorder("Chat : "));
		sp.setPreferredSize(new Dimension(w-h-40,h/3));
		bv.add(pseudoLabel);
		bv.add(separation);
		bv.add(sp);
		bv.add(separation2);
		
		Box bh = Box.createHorizontalBox();
		Box bh2 = Box.createHorizontalBox();
		bh.add(ron);
		ron.setEnabled(false);
		ron.addActionListener(this);
		bh.add(pon);
		pon.setEnabled(false);
		pon.addActionListener(this);
		bh.add(chi);	
		chi.setEnabled(false);
		chi.addActionListener(this);
		bh2.add(help);
		help.addActionListener(this);
		bh2.add(rules);
		rules.addActionListener(this);
		bv.add(bh);
		bv.add(separation3);
		
		discardPanel = new JPanel();
		discardPanel.setLayout(null);
		discardPanel.setPreferredSize(new Dimension(0,h/3));
		discardPanel.setBorder(BorderFactory.createTitledBorder("Défausse : "));
		bv.add(discardPanel);
		bv.add(separation4);
		bv.add(bh2);
		
		this.add(bv);
	}
	
	public JLabel getPseudoLabel() {
		return pseudoLabel;
	}
	public void setPseudoLabel(JLabel pseudoLabel) {
		this.pseudoLabel = pseudoLabel;
	}
	public JTextArea getChat() {
		return chat;
	}
	public void setChat(JTextArea chat) {
		this.chat = chat;
	}
	public JButton getPon() {
		return pon;
	}
	public void setPon(JButton pon) {
		this.pon = pon;
	}
	public JButton getRon() {
		return ron;
	}
	public void setRon(JButton ron) {
		this.ron = ron;
	}
	public JButton getChi() {
		return chi;
	}
	public void setChi(JButton chi) {
		this.chi = chi;
	}
	
	public JPanel getDiscardPanel() {
		return discardPanel;
	}
	public void setDiscardPanel(JPanel discardPanel) {
		this.discardPanel = discardPanel;
	}
	
	public void switchPseudo(String pseudo){
		pseudoLabel.setText("Joueur actuel : " + pseudo);
	}
	
	/**
	 * Ajouter une tuile à la défausse du joueur.
	 * 
	 * @param t tuile à défausser.
	 */
	public void addDiscardedTile(Tile t){
		TileComponent tc = new TileComponent(t, 'S', true);
		int tileSize = TileUtil.tileSizeDP;
		tc.setSize(tileSize, tileSize);
		tc.setLocation((nbTile - ((nbTile/5)*5))*56 - tileSize/5 + 10, (nbTile/5)*tileSize + 20);
		tc.setDraggable(false);
		discardPanel.add(tc);
		discardPanel.repaint();
		
		nbTile++;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == chi){
			pon.setEnabled(false);
			ron.setEnabled(false);
			chi.setEnabled(false);
			mc.buttonControl(null, 0);
		}
		if(e.getSource() == pon){
			ron.setEnabled(false);
			chi.setEnabled(false);
			pon.setEnabled(false);
			mc.buttonControl(null, 1);
		}
		if(e.getSource() == help){
			new HelpFrame();
		}
		if(e.getSource() == rules){
			new RulesFrame();
		}
		if(e.getSource() == ron){
			chi.setEnabled(false);
			pon.setEnabled(false);
			ron.setEnabled(false);
			mc.buttonControl(null, 2);
		}
	}
}
