package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import Util.TileUtil;
import controller.MahjongController;
import model.Player;
import model.Tile;
/**
 * panel drag and drop pour le joueur.
 * 
 * @author Martin
 *
 */
@SuppressWarnings("serial")
public class DragDropPanel extends JPanel implements MouseListener, ActionListener{
	private Player player;
	private ArrayList<Tile> tiles;
	private ArrayList<TileComponent> tilesComponent = new ArrayList<TileComponent>();
	private JPopupMenu popup = new JPopupMenu();
	private JMenuItem discard = new JMenuItem("Defausser cette tuile");
	private TileComponent selectedTile;
	private MahjongController mc;
	
	public DragDropPanel(MahjongController mc) {
		super();
		
		this.mc = mc;
		
		this.setLayout(null);
		this.setBackground(Color.BLACK);
		
		popup.add(discard);
		discard.addActionListener(this);
		discard.setEnabled(false);
	}
	public ArrayList<Tile> getTiles() {
		return tiles;
	}
	
	/**
	 * Ajoute la liste de tuile à la main et les affiche.
	 * 
	 * @param tiles liste de tuile à afficher.
	 */
	public void setTiles(ArrayList<Tile> tiles) {
		this.tiles = new ArrayList<Tile>(tiles);
		
		for(int i = 0; i < tiles.size(); i ++){
			if(i < 7) paintTile(tiles.get(i), 's',((i+1)*80),5);
			else paintTile(tiles.get(i), 's',((i-6)*80),110);
		}
	}
	/**
	 * Ajoute une tuile dans la main et l'affiche.
	 * 
	 * @param newTile tuile à afficher.
	 */
	public void addTiles(Tile newTile) {
		tiles.add(newTile);
		paintTile(newTile, 's',0,5);
	}
	/**
	 * Supprime une tuile en fonction de l'id de la tuile.
	 * 
	 * @param index id de la tuile à suppripmer.
	 */
	public void removeTile(int index){
		this.remove(tilesComponent.get(index));
		tilesComponent.remove(index);
	}
	/**
	 * Supprime une tuile.
	 * 
	 * @param tile tuile à supprimer.
	 */
	public void removeTile(Tile tile){
		for(int i = 0; i < tilesComponent.size() ; i++){
			if(tilesComponent.get(i).getTile().equals(tile)){
				tiles.remove(tile);
				this.remove(tilesComponent.get(i));
				tilesComponent.remove(i);
				this.repaint();
			}
		}
	}

	/**
	 * Affiche une tuile à la position x,y avec la bonne direction.
	 * 
	 * @param t tuile à afficher.
	 * @param direction direction dans laquelle afficher la tuile.
	 * @param x x
	 * @param y y
	 */
	public void paintTile(Tile t, char direction, int x, int y){
		TileComponent tc = new TileComponent(t, direction, true);
		tc.addMouseListener(this);
		tc.setSize(TileUtil.tileSizeHand,TileUtil.tileSizeHand);
		tc.setLocation(x,y);
		tilesComponent.add(tc);
		this.add(tc);
		this.repaint();
	}
	
	public void enableDiscardTile(){
		discard.setEnabled(true);
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
	}
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON3){
			popup.show((TileComponent)e.getSource(), e.getX(), e.getY());
			selectedTile = (TileComponent)e.getSource();
		}
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(selectedTile != null){
			discard.setEnabled(false);
			mc.discardTile(selectedTile.getTile());
		}
	}
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
}
