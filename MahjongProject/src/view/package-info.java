/**
 * Ce package contient toutes les classes graphiques :
 * <ul>
 * <li>La classe <code>ConnectionFrame.java</code></li>
 * <li>La classe <code>DragDropPanel.java</code></li>
 * <li>La classe <code>DraggableComponent.java</code></li>
 * <li>La classe <code>DraggaleImageComponent.java</code></li>
 * <li>La classe <code>GameFrame.java</code></li>
 * <li>La classe <code>HelpFrame.java</code></li>
 * <li>La classe <code>LobbyFrame.java</code></li>
 * <li>La classe <code>MenuFrame.java</code></li>
 * <li>La classe <code>OpponentPanel.java</code></li>
 * <li>La classe <code>PlayerPanel.java</code></li>
 * <li>La classe <code>RulesFrame.java</code></li>
 * <li>La classe <code>SidePanel.java</code></li>
 * <li>La classe <code>TablePanel.java</code></li>
 * <li>La classe <code>TileComponent.java</code></li>
 * <li>La classe <code>VictoryFrame.java</code></li>
 * <li>La classe <code>VisibleTilePanel.java</code></li>
 * <li>La classe <code>WallPanel.java</code></li>
 * </ul>
 * 
 * @author Dylan Brossel
 */
package view;