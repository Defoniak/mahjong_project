package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Player;
import model.Tile;

import org.junit.Test;

import Util.TileUtil;

public class TestPlayer {
	private ArrayList<Tile> deck = TileUtil.initDeck();
	private Player player = new Player("test");
	
	@Test
	public void testIsVictory() {
		ArrayList<Tile> hand = new ArrayList<Tile>();
		hand.add(deck.get(0));
		hand.add(deck.get(1));
		hand.add(deck.get(4));
		hand.add(deck.get(5));
		hand.add(deck.get(8));
		hand.add(deck.get(9));
		hand.add(deck.get(12));
		hand.add(deck.get(13));
		hand.add(deck.get(16));
		hand.add(deck.get(17));
		hand.add(deck.get(20));
		hand.add(deck.get(21));
		hand.add(deck.get(24));
		hand.add(deck.get(25));
		player.setHand(hand);
		assertEquals(true, player.isVictory()); // 7 paires
		
		hand.removeAll(hand);
		hand.add(deck.get(0));
		hand.add(deck.get(1));
		hand.add(deck.get(2));
		hand.add(deck.get(4));
		hand.add(deck.get(5));
		hand.add(deck.get(6));
		hand.add(deck.get(8));
		hand.add(deck.get(9));
		hand.add(deck.get(10));
		hand.add(deck.get(12));
		hand.add(deck.get(13));
		hand.add(deck.get(14));
		hand.add(deck.get(16)); // paire
		hand.add(deck.get(17)); // paire
		player.setHand(hand);
		assertEquals(true, player.isVictory());
		
		hand.removeAll(hand);
		hand.add(deck.get(0));
		hand.add(deck.get(4));
		hand.add(deck.get(8));
		hand.add(deck.get(12));
		hand.add(deck.get(16));
		hand.add(deck.get(20));
		hand.add(deck.get(24));
		hand.add(deck.get(28));
		hand.add(deck.get(32));
		hand.add(deck.get(36));
		hand.add(deck.get(40));
		hand.add(deck.get(44));
		hand.add(deck.get(48));
		hand.add(deck.get(52));
		player.setHand(hand);
		assertEquals(false, player.isVictory()); //Main non gagnante
	}

	@Test
	public void testCanPon() {
		Tile lastTile = deck.get(0);
		ArrayList<Tile> hand = new ArrayList<Tile>();
		hand.add(deck.get(1));
		hand.add(deck.get(2));
		hand.add(deck.get(4));
		hand.add(deck.get(5));
		hand.add(deck.get(8));
		hand.add(deck.get(9));
		hand.add(deck.get(12));
		hand.add(deck.get(13));
		hand.add(deck.get(16));
		hand.add(deck.get(17));
		hand.add(deck.get(20));
		hand.add(deck.get(21));
		hand.add(deck.get(24));
		player.setHand(hand);
		assertEquals(true, player.canPon(lastTile)); //Pon de chiffre
		
		hand.removeAll(hand);
		lastTile = deck.get(125);
		hand.add(deck.get(1));
		hand.add(deck.get(2));
		hand.add(deck.get(4));
		hand.add(deck.get(5));
		hand.add(deck.get(8));
		hand.add(deck.get(9));
		hand.add(deck.get(12));
		hand.add(deck.get(13));
		hand.add(deck.get(16));
		hand.add(deck.get(17));
		hand.add(deck.get(20));
		hand.add(deck.get(126));
		hand.add(deck.get(127));
		player.setHand(hand);
		assertEquals(true, player.canPon(lastTile)); //Pon d'honneurs
		
		hand.removeAll(hand);
		lastTile = deck.get(103);
		hand.add(deck.get(1));
		hand.add(deck.get(2));
		hand.add(deck.get(4));
		hand.add(deck.get(5));
		hand.add(deck.get(8));
		hand.add(deck.get(9));
		hand.add(deck.get(12));
		hand.add(deck.get(13));
		hand.add(deck.get(16));
		hand.add(deck.get(17));
		hand.add(deck.get(20));
		hand.add(deck.get(126));
		hand.add(deck.get(127));
		player.setHand(hand);
		assertEquals(false, player.canPon(lastTile)); //Pas de Pon
	}

	@Test
	public void testCanChi() {
		Tile lastTile = deck.get(0);
		ArrayList<Tile> hand = new ArrayList<Tile>();
		hand.add(deck.get(1));
		hand.add(deck.get(4));
		hand.add(deck.get(2));
		hand.add(deck.get(5));
		hand.add(deck.get(8));
		hand.add(deck.get(9));
		hand.add(deck.get(12));
		hand.add(deck.get(13));
		hand.add(deck.get(16));
		hand.add(deck.get(17));
		hand.add(deck.get(20));
		hand.add(deck.get(21));
		hand.add(deck.get(24));
		player.setHand(hand);
		assertEquals(true, player.canChi(lastTile)); //Chi de chiffres
		
		hand.removeAll(hand);
		hand.add(deck.get(1));
		hand.add(deck.get(2));
		hand.add(deck.get(4));
		hand.add(deck.get(5));
		hand.add(deck.get(8));
		hand.add(deck.get(9));
		hand.add(deck.get(12));
		hand.add(deck.get(13));
		hand.add(deck.get(16));
		hand.add(deck.get(17));
		hand.add(deck.get(20));
		hand.add(deck.get(21));
		hand.add(deck.get(24));
		player.setHand(hand);
		assertEquals(true, player.canChi(lastTile)); //Autre Chi
		
		hand.removeAll(hand);
		lastTile = deck.get(1);
		hand.add(deck.get(0));
		hand.add(deck.get(2));
		hand.add(deck.get(4));
		hand.add(deck.get(5));
		hand.add(deck.get(8));
		hand.add(deck.get(9));
		hand.add(deck.get(12));
		hand.add(deck.get(13));
		hand.add(deck.get(16));
		hand.add(deck.get(17));
		hand.add(deck.get(20));
		hand.add(deck.get(21));
		hand.add(deck.get(24));
		player.setHand(hand);
		assertEquals(true, player.canChi(lastTile)); //Autre Chi
		
		hand.removeAll(hand);
		lastTile = deck.get(123);
		hand.add(deck.get(0));
		hand.add(deck.get(2));
		hand.add(deck.get(4));
		hand.add(deck.get(5));
		hand.add(deck.get(8));
		hand.add(deck.get(9));
		hand.add(deck.get(12));
		hand.add(deck.get(13));
		hand.add(deck.get(16));
		hand.add(deck.get(17));
		hand.add(deck.get(20));
		hand.add(deck.get(21));
		hand.add(deck.get(24));
		player.setHand(hand);
		assertEquals(false, player.canChi(lastTile)); //Pas de Chi
	}

}
