package model;

/**
 * Les vents.
 * 
 * @author Martin
 *
 */
public class Wind extends Honor{
	private char direction;
	
	public Wind(char direction, String image) {
		super(image);
		this.setDirection(direction);
	}
	public char getDirection() {
		return direction;
	}
	public void setDirection(char direction) {
		this.direction = direction;
	}
}
