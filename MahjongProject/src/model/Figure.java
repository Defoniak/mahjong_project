package model;

/**
 * Les chiffres.
 * 
 * @author Martin
 *
 */
public class Figure extends Tile{
	private int value = 0;
	private String family = "";

	public Figure(int value, String family, String image){
		super(image);
		this.setValue(value);
		this.setFamily(family);
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getFamily() {
		return family;
	}
	
	/**
	 * Renvoit la famille en français pour un bel affichage.
	 * 
	 * @return la famille.
	 */
	public String getFamille() {
		String famille = "";
		switch(((Figure)this).getFamily()){
			case "bamboo" : famille = "bambou"; break;
			case "character" : famille = "caractère"; break;
			case "circle" : famille = "cercle"; break;
		}
		return famille;
	}

	public void setFamily(String family) {
		this.family = family;
	}
	
	/**
	 * compare deux Figure au niveaux de la famille.
	 * 
	 * @param f la Figure à comparer.
	 * @return true si les deux Figure ont la même famille.
	 */
	public boolean isSameFamily(Figure f){
		return(this.getFamily().equals(f.getFamily()));
	}
}
