/**
 * Ce package contient toutes les classes des modèles utilisés :
 * <ul>
 * <li>La classe des dragons <code>Dragon.java</code></li>
 * <li>La classe des chiffres <code>Figure.java</code></li>
 * <li>La classe des honneurs <code>Honor.java</code></li>
 * <li>La classe des joueurs <code>Player.java</code></li>
 * <li>La classe de la table <code>Table.java</code></li>
 * <li>La classe des tuiles <code>Tile.java</code></li>
 * <li>La classe des vents <code>Wind.java</code></li>
 * </ul>
 * 
 * @author Dylan Brossel
 */
package model;